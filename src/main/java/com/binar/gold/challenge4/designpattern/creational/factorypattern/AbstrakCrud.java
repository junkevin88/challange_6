package com.binar.gold.challenge4.designpattern.creational.factorypattern;

import java.util.Map;

public abstract  class AbstrakCrud {
    public abstract  Map save(Object req);
    public abstract  Map update(Object req);
    public abstract  Map delete(Object req);
    public abstract  Map list(Object req);
}
