package com.binar.gold.challenge4.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="seats")
@Entity
@Where(clause = "deleted_date is null")
public class Seats extends AbstractDate implements Serializable {
    @EmbeddedId
    private SeatId seatId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "scheduling_film_id")
    private ScheduleFilm scheduleFilm;



}
