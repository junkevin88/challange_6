package com.binar.gold.challenge4.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="schedules")
@Entity
@Where(clause = "deleted_date is null")
public class Schedules extends AbstractDate implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "schedule_id")
    private Long id;


    @Column(name = "ticket_price")
    private Float price;

//    @Transient // Objek tidak akan di buatkan kolumn ke tabel database
//    private String priceRupiah;
//    public String getPrice() {
//        if(getPrice() !=null){
//            NumberFormat nf = NumberFormat.getNumberInstance(new Locale("in", "ID"));
//            return "Rp. "+nf.format(price);
//        }
//        return "Rp. 0";
//    }

//    @DateTimeFormat(pattern = "dd-MM-yy")
    @Column(name = "date_scheduled")
    private LocalDate dateScheduled;

    @Column(name = "date_started")
    private LocalTime dateStarted;

    @Column(name = "date_ended")
    private LocalTime dateEnded;


}
