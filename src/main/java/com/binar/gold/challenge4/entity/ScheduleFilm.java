package com.binar.gold.challenge4.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="schedule_film")
@Entity
@Where(clause = "deleted_date is null")
public class ScheduleFilm extends AbstractDate implements Serializable {
    @Column(name = "film_schedule_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Films films;

    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private Schedules schedules;

}
