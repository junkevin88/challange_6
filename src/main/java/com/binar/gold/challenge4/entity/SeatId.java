package com.binar.gold.challenge4.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class SeatId implements Serializable {
    @Column(name = "seat_number")
    private Integer seatNumber;

    @Column(name = "studio_name")
    private String studioName;

}
