package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Map;

public interface ScheduleRepository extends JpaRepository<Schedules, Long> {

    @Query(value = "select s from Schedules s WHERE s.id = :idSchedules", nativeQuery = false)
    public Schedules getById(@Param("idSchedules") Long id);

    @Query(value = "select s from Schedules s ", nativeQuery = false)
    public Page<Schedules> getListData(Pageable pageable);

//    @Query("FROM Users u WHERE LOWER(u.username) like LOWER(:username)")
//    public Page<Users> findByUsernameLike(String username, Pageable pageable);
//    public Page<Users> findByEmailLike(String  email, Pageable pageable);
//
//    public Page<Users> findByUsernameLikeAndEmailLike(String username, String  email, Pageable pageable);
//



}
