package com.binar.gold.challenge4.repository.oauth;

import com.binar.gold.challenge4.entity.oauth.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
    @Query("FROM Person p WHERE LOWER(p.username) = LOWER(?1)")
    Person findOneByUsername(String username);

    @Query("FROM Person p WHERE p.otp = ?1")
    Person findOneByOTP(String otp);

    @Query("FROM Person p WHERE LOWER(p.username) = LOWER(:username)")
    Person checkExistingEmail(String username);
}
