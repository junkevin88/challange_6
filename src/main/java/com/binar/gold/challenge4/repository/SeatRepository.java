package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.entity.SeatId;
import com.binar.gold.challenge4.entity.Seats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SeatRepository extends JpaRepository<Seats, SeatId> {

    @Query(value = "select t from Seats t WHERE t.seatId.seatNumber = :idSeat", nativeQuery = false)
    public Seats getBySeatId(@Param("idSeat") SeatId seatId);
//    @Query(value = "select t from Seats t WHERE t.seatId.studioName = :idStudioName", nativeQuery = false)
//    public Seats getBySeatIdStudioName(@Param("idStudioName") SeatId seatId);


    @Query(value = "select t from Seats t", nativeQuery = false)
    public Page<Seats> getListData(Pageable pageable);
}
