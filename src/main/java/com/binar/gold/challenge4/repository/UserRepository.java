package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<Users, Long> {
    @Query(value = "select u from Users u WHERE u.id = :idUsers", nativeQuery = false)
    public Users getById(@Param("idUsers") Long id);

    @Query(value = "select u from Users u ", nativeQuery = false)
    public Page<Users> getListData(Pageable pageable);

    @Query("FROM Users u WHERE LOWER(u.username) like LOWER(:username)")
    public Page<Users> findByUsernameLike(String username, Pageable pageable);
    public Page<Users> findByEmailLike(String  email, Pageable pageable);

    public Page<Users> findByUsernameLikeAndEmailLike(String username, String  email, Pageable pageable);



}
