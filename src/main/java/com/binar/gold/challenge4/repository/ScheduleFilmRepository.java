package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.ScheduleFilm;
import com.binar.gold.challenge4.entity.Schedules;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ScheduleFilmRepository extends JpaRepository<ScheduleFilm, Long> {
    @Query(value = "select s from ScheduleFilm s ", nativeQuery = false)
    public Page<ScheduleFilm> getListData(Pageable pageable);

    @Query(value = "select s from ScheduleFilm s WHERE s.films.id = :idFilms", nativeQuery = false)
    public ScheduleFilm getByFilmId(@Param("idFilms") Long id);

}
