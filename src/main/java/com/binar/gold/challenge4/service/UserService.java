package com.binar.gold.challenge4.service;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Users;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public interface UserService {


//    public Iterable<Users> findAll();
//    public Map update(Users obj);
//    public void delete(Long id);
//    public Map getById(Long obj);

//    public Users findOne(Long id);
//
//    public void deleteOne(Long id);


    public Map save(Users request);

    public  Map update(Users request);

    public Map delete(Long id);
}
