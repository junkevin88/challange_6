package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.entity.SeatId;
import com.binar.gold.challenge4.entity.Seats;
import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.SeatRepository;
import com.binar.gold.challenge4.repository.UserRepository;
import com.binar.gold.challenge4.service.FilmService;
import com.binar.gold.challenge4.service.SeatService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;


@Service
public class SeatImpl  implements SeatService {
    private static Logger logger = LoggerFactory.getLogger(SeatImpl.class);
    @Autowired
    public SeatRepository seatRepository;

    @Autowired
    public Response response;

    @Override
    public Map save(Seats request) {
        try {

//            if (request.getSeatNumber() == null) {
//                return response.error("Seat Number is required!", Config.ERROR_401);
////            }
//            if (request.getStudioName() == null) {
//                return response.error("Studio Name is required!", Config.ERROR_401);
//            }
            Seats doSave = seatRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }


    @Override
    public Map update(Seats request) {
        try {

            if (request.getSeatId() == null) {
                return response.error("Seat Number and Studio Name is required!", Config.ERROR_401);
            }
            Seats checkingData = seatRepository.getBySeatId(request.getSeatId());
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setUser(request.getUser());
            checkingData.setScheduleFilm(request.getScheduleFilm());

            Seats doSave = seatRepository.save(checkingData);
            return response.sukses(doSave);

        } catch (Exception e) {
            logger.error("Error update, {} " + e);
            return response.error("Error update: " + e.getMessage(), Config.ERROR_500);
        }
    }


//    @Override
//    public Iterable<Users> findAll() {
//        return userRepository.findAll();
//    }

//    @Override
//    public Map delete(Integer seatNumber, String studioName) {
//        try {
//            if (seatNumber == null) {
//                return response.error("Seat number is required!", Config.ERROR_401);
//            }
//
//            if (studioName == null) {
//                return response.error("Studio Name is required!", Config.ERROR_401);
//            }
//            Seats checkingData =seatRepository.getBySeatIdSeatNumber(seatNumber);
//            if (checkingData == null) {
//                return response.error("Data cannot be found!", Config.ERROR_404);
//            }
//            checkingData.setDeleted_date(new Date());
//            Seats saveDeleted= seatRepository.save(checkingData);
//            return response.sukses("sukses");
//        } catch (Exception e) {
//            logger.error("Error delete, {} " + e);
//            return response.error("Error delete: " + e.getMessage(), Config.ERROR_500);
//        }
//    }
//


}
