package com.binar.gold.challenge4.service;

import com.binar.gold.challenge4.entity.Films;

import java.util.Map;

public interface FilmService {
//    public Films save(Films films);
//
//    public Iterable<Films> findAll();
//    //    public Map update(Users obj);
//    public void delete(Long films);
////    public Map getById(Long obj);
//
//    public Films findOne(Long id);
//
//    public void deleteOne(Long id);
//
    public Map save(Films request);

    public  Map update(Films request);

    public Map delete(Long id);

    public Map getById(Long request);



    public Iterable<Films> getFilmsFilterStatus();


}
