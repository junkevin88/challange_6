package com.binar.gold.challenge4.service.oauth;

import com.binar.gold.challenge4.entity.oauth.Person;
import com.binar.gold.challenge4.repository.oauth.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class Oauth2UserDetailsService implements UserDetailsService{

    @Autowired
    private PersonRepository personRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Person person = personRepository.findOneByUsername(s);
        if (null == person) {
            throw new UsernameNotFoundException(String.format("Username %s is not found", s));
        }

        return person;
    }

    @CacheEvict("oauth_username")
    public void clearCache(String s) {
    }
}
