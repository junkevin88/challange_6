package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.service.FilmService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.lang.Boolean.TRUE;

@Service
public class FilmImpl implements FilmService {


    private static Logger logger = LoggerFactory.getLogger(FilmImpl.class);
    @Autowired
    public FilmRepository filmRepository;

    @Autowired
    public Response response;


    @Override
    public Map save(Films request) {
        try {
            if (request.getName() == null) {
                return response.error("Name is required!", Config.ERROR_401);
            }
            Films doSave = filmRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map update(Films request) {
        try {
            if (request.getId() == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(request.getId());
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setStatus(request.getStatus());
            checkingData.setName(request.getName());
            Films doSave = filmRepository.save(checkingData);
            return response.sukses(doSave);

        } catch (Exception e) {
            logger.error("Error update, {} " + e);
            return response.error("Error update: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            if (id == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(id);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setDeleted_date(new Date());
            Films saveDeleted= filmRepository.save(checkingData);
            return response.sukses("sukses");
        } catch (Exception e) {
            logger.error("Error delete, {} " + e);
            return response.error("Error delete: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map getById(Long request) {
        try {
            if (request == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(request);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }
            return response.sukses(checkingData);

        } catch (Exception e) {
            logger.error("Error get by id, {} " + e);
            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
        }

    }

    @Override
    public Iterable<Films> getFilmsFilterStatus() {
        return filmRepository.getFilmsFilterStatus();
    }



//    @Override
//    public Map getFilmsFilterStatus(Long request) {
//        try {
//
//            List<Films> checkingData = filmRepository.getFilmsFilterStatus(request);
//            if (checkingData == null) {
//                return response.error("Data cannot be found!", Config.ERROR_404);
//            }
//            return response.sukses(checkingData);
//
//        } catch (Exception e) {
//            logger.error("Error get by id, {} " + e);
//            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
//        }
//    }

}
