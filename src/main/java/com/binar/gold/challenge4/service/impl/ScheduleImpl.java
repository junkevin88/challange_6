package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.repository.ScheduleRepository;
import com.binar.gold.challenge4.service.ScheduleService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class ScheduleImpl implements ScheduleService {

    private static Logger logger = LoggerFactory.getLogger(ScheduleImpl.class);
    @Autowired
    public ScheduleRepository scheduleRepository;

    @Autowired
    public Response response;

    @Override
    public Map save(Schedules request) {
        try {
            if (request.getDateScheduled() == null) {
                return response.error("Date scheduled is required!", Config.ERROR_401);
            }
            if (request.getDateStarted() == null) {
                return response.error("Date started is required!", Config.ERROR_401);
            }
            if (request.getDateEnded() == null) {
                return response.error("Date ended is required!", Config.ERROR_401);
            }
            Schedules doSave = scheduleRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map update(Schedules request) {

        try {
            if (request.getId() == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Schedules checkingData = scheduleRepository.getById(request.getId());
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setPrice(request.getPrice());
            checkingData.setDateScheduled(request.getDateScheduled());
            checkingData.setDateStarted(request.getDateStarted());
            checkingData.setDateEnded(request.getDateEnded());

            Schedules doSave = scheduleRepository.save(checkingData);
            return response.sukses(doSave);

        } catch (Exception e) {
            logger.error("Error update, {} " + e);
            return response.error("Error update: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            if (id == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Schedules checkingData = scheduleRepository.getById(id);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }
            checkingData.setDeleted_date(new Date());
            Schedules saveDeleted = scheduleRepository.save(checkingData);
            return response.sukses("sukses");
        } catch (Exception e) {
            logger.error("Error delete, {} " + e);
            return response.error("Error delete: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map getById(Long request) {
        try {
            if (request == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Schedules checkingData = scheduleRepository.getById(request);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }
            return response.sukses(checkingData);

        } catch (Exception e) {
            logger.error("Error get by id, {} " + e);
            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
        }

    }

}
