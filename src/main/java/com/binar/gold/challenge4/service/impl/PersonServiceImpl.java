package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.config.Config;
import com.binar.gold.challenge4.dao.request.LoginModel;
import com.binar.gold.challenge4.dao.request.RegisterModel;
import com.binar.gold.challenge4.entity.oauth.Person;
import com.binar.gold.challenge4.entity.oauth.Role;
import com.binar.gold.challenge4.repository.oauth.PersonRepository;
import com.binar.gold.challenge4.repository.oauth.RoleRepository;
import com.binar.gold.challenge4.service.oauth.Oauth2UserDetailsService;
import com.binar.gold.challenge4.service.person.PersonService;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonServiceImpl implements PersonService {

    public static final Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);
    Config config = new Config();
    private static final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);
    @Autowired
    RoleRepository repoRole;

    @Autowired
    PersonRepository repoUser;
    @Autowired
    private Oauth2UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    public Response response;

    @Autowired
    public Response responses,templateResponse;

    @Value("${BASEURL}")
    private String baseUrl;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public Map login(LoginModel loginModel) {
        /**
         * bussines logic for login here
         * **/
        try {

            if (loginModel.getUsername() == "") {
                return response.Error("Fill the username");
            }

            if (loginModel.getPassword() == "") {
                return response.Error("Fill the password");
            }

            if(response.chekNull(loginModel.getUsername())) {
                return response.Error("Email is required");
            }

            if(response.chekNull(loginModel.getPassword())) {
                return response.Error("Password is required");
            }

            Map<String, Object> map = new HashMap<>();

            Person checkUser = repoUser.findOneByUsername(loginModel.getUsername());

//            if (checkUser.isBlocked()) {
//                return response.Error("Your account is blocked, please contact Admin!");
//            }

            if ((checkUser != null) && (encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                if (!checkUser.isEnabled()) {
                    map.put("is_enabled", checkUser.isEnabled());
                    return response.Error(map);
                }
            }
            if (checkUser == null) {
                return response.notFound("User not found");
            }
            if (!(encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                return response.Error("Wrong Password");
            }
            String url = baseUrl + "/oauth/token?username=" + loginModel.getUsername() +
                    "&password=" + loginModel.getPassword() +
                    "&grant_type=password" +
                    "&client_id=my-client-web" +
                    "&client_secret=password";
            ResponseEntity<Map> response = restTemplateBuilder.build().exchange(url, HttpMethod.POST, null, new
                    ParameterizedTypeReference<Map>() {
                    });

            if (response.getStatusCode() == HttpStatus.OK) {
                Person user = repoUser.findOneByUsername(loginModel.getUsername());
                List<String> roles = new ArrayList<>();

                for (Role role : user.getRoles()) {
                    roles.add(role.getName());
                }
                //save token
//                checkUser.setAccessToken(response.getBody().get("access_token").toString());
//                checkUser.setRefreshToken(response.getBody().get("refresh_token").toString());
//                userRepository.save(checkUser);

                repoUser.save(user);


                map.put("access_token", response.getBody().get("access_token"));
                map.put("token_type", response.getBody().get("token_type"));
                map.put("refresh_token", response.getBody().get("refresh_token"));
                map.put("expires_in", response.getBody().get("expires_in"));
                map.put("scope", response.getBody().get("scope"));
                map.put("jti", response.getBody().get("jti"));
                map.put("user", user);


                return responses.Sukses("map");
            } else {
                return null;
            }
        } catch (HttpStatusCodeException e) {
            e.printStackTrace();
            if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                return response.Error("invalid login");
            }
            return response.Error(e);
        } catch (Exception e) {
            e.printStackTrace();

            return response.Error(e);
        }
    }

    @Override
    public Map registerManual(RegisterModel objModel) {
        Map map = new HashMap();
        try {
            String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // admin
            Person user = new Person();
            user.setUsername(objModel.getEmail().toLowerCase());

            //step 1 :
            user.setEnabled(false); // matikan user

            String password = encoder.encode(objModel.getPassword().replaceAll("\\s+", ""));
            List<Role> r = repoRole.findByNameIn(roleNames);

            user.setRoles(r);
            user.setPassword(password);
            Person obj = repoUser.save(user);

            return templateResponse.templateSukses("Sukses");

        } catch (Exception e) {
            logger.error("Eror registerManual=", e);
            return templateResponse.templateEror("eror:"+e);
        }    }


}


