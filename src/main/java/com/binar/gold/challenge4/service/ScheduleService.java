package com.binar.gold.challenge4.service;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Schedules;

import java.util.Map;

public interface ScheduleService {
    public Map save(Schedules request);

    public  Map update(Schedules request);

    public Map delete(Long id);

    public Map getById(Long request);


}
