package com.binar.gold.challenge4.service.person;



import com.binar.gold.challenge4.dao.request.LoginModel;
import com.binar.gold.challenge4.dao.request.RegisterModel;

import java.security.Principal;
import java.util.Map;

public interface PersonService {

    public Map login(LoginModel objLogin);

    Map registerManual(RegisterModel objModel) ;
}
