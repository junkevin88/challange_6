package com.binar.gold.challenge4.testing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FilmTesting {
    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void konversiJsonKeString() throws JsonProcessingException {
        String json = "{\n" +
                "    \"nama\":\"nama 2\",\n" +
                "    \"stok\":200,\n" +
                "    \"satuan\":\"kg\",\n" +
                "    \"harga\":\"100000\",\n" +
                "    \"supplier\":{\n" +
                "        \"id\":123\n" +
                "    }\n" +
                "}";
        ObjectMapper objectMapper = new ObjectMapper();

//        JsonNode jsonNode = objectMapper.readTree(json);
//
//        System.out.println(jsonNode.get("f1").asText());
//        System.out.println(jsonNode);
    }

    @Test
    public void insertSupplier() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        String bodyTesting = "{\n" +
                "    \"name\":\"supplier 2sss\",\n" +
                "    \"hp\":\"098765462\",\n" +
                "    \"alamat\":\"jl sudirman2\"\n" +
                "}";
//        Map bodyTesting = new Ma

        HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);

        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:9091/api/film/save", HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response  =" + exchange.getBody());
    }
}
