package com.binar.gold.challenge4.controller;

import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.UserRepository;
import com.binar.gold.challenge4.service.UserService;
import com.binar.gold.challenge4.utils.Response;
import com.binar.gold.challenge4.utils.SimpleStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public Response response;

    @Autowired
    public UserRepository userRepository;
    @Autowired
    private UserService userService;

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Users users) {
//        if(errors.hasErrors()){
//            for (ObjectError error : errors.getAllErrors()) {
//                System.err.println(error.getDefaultMessage());
//            }
//            throw new RuntimeException("Validation Error");
//        }

        return new ResponseEntity<Map>(userService.save(users), HttpStatus.OK);
    }


//    @GetMapping("/list")
//    public Iterable<Users> findAll(){
//        return userService.findAll();
//    }


    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Users users){
        return new ResponseEntity<Map>(userService.update(users), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(userService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> listUsers(
            @RequestParam() Integer page,
            @RequestParam(required = true) Integer size,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String ordertype) {
        Pageable show_data = simpleStringUtils.getShort(orderby, ordertype, page, size);
        Page<Users> list = null;

        if(username != null && !username.isEmpty() && email != null && !email.isEmpty() ){
            list = userRepository.findByUsernameLikeAndEmailLike("%"+username+"%","%"+email+"%",show_data);
        }else if(email != null && !email.isEmpty()){
            list = userRepository.findByEmailLike("%"+email+"%",show_data);
        }else if(username != null && !username.isEmpty()){
            list = userRepository.findByUsernameLike("%"+username+"%",show_data);
        }else {
            // nampilkan semuanya
            list = userRepository.getListData(show_data);
        }
        return new ResponseEntity<Map>(response.sukses(list), new HttpHeaders(), HttpStatus.OK);
    }
}
