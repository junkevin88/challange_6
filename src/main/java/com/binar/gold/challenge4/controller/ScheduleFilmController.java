package com.binar.gold.challenge4.controller;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.ScheduleFilm;
import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.repository.ScheduleFilmRepository;
import com.binar.gold.challenge4.repository.ScheduleRepository;
import com.binar.gold.challenge4.service.FilmService;
import com.binar.gold.challenge4.service.ScheduleFilmService;
import com.binar.gold.challenge4.service.ScheduleService;
import com.binar.gold.challenge4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/schedulefilm")
public class ScheduleFilmController {
    @Autowired
    public Response response;

    @Autowired
    public ScheduleFilmRepository scheduleFilmRepository;


    @Autowired
    private ScheduleFilmService scheduleFilmService;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody ScheduleFilm schedulefilm) {
        return new ResponseEntity<Map>(scheduleFilmService.save(schedulefilm), HttpStatus.OK);
    }

    @GetMapping("/list")
    public Iterable<ScheduleFilm> findAll(){
        return scheduleFilmRepository.findAll();
    }

    @PostMapping("/search/{id}")
    public ResponseEntity<Map> getId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(scheduleFilmService.getByFilmId(id), HttpStatus.OK);
    }


}
