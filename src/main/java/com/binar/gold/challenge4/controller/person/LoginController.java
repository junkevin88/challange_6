package com.binar.gold.challenge4.controller.person;

import com.binar.gold.challenge4.config.Config;
import com.binar.gold.challenge4.dao.request.LoginModel;
import com.binar.gold.challenge4.repository.oauth.PersonRepository;
import com.binar.gold.challenge4.service.person.PersonService;
import com.binar.gold.challenge4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/user-login/")
public class LoginController {
    @Autowired
    private PersonRepository personRepository;

    Config config = new Config();

    @Autowired
    public PersonService personService;



    @Autowired
    public Response templateCRUD;

    @PostMapping("/login")
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> login(@Valid @RequestBody LoginModel objModel) {
        Map map = personService.login(objModel);
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

}
