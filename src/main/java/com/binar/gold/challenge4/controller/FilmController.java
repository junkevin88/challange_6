package com.binar.gold.challenge4.controller;


import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.service.FilmService;
import com.binar.gold.challenge4.utils.Response;
//import org.jetbrains.annotations.NotNull;
import com.binar.gold.challenge4.utils.SimpleStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.validation.Errors;
//import org.springframework.validation.ObjectError;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//import javax.validation.Valid;

@RestController
@RequestMapping("/film")
public class FilmController {

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();

    @Autowired
    public Response response;

    @Autowired
    public FilmRepository filmRepository;
    @Autowired
    private FilmService filmService;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Films films) {
//        if(errors.hasErrors()){
//            for (ObjectError error : errors.getAllErrors()) {
//                System.err.println(error.getDefaultMessage());
//            }
//            throw new RuntimeException("Validation Error");
//        }

        return new ResponseEntity<Map>(filmService.save(films), HttpStatus.OK);
    }


    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Films films){
        return new ResponseEntity<Map>(filmService.update(films), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Map> getId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.getById(id), HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/status")
    public Iterable<Films> getFilmsFilterStatus(){
        return filmRepository.getFilmsFilterStatus();
    }

    @GetMapping("/list")
    public ResponseEntity<Map> listFilms(
            @RequestParam() Integer page,
            @RequestParam(required = true) Integer size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String ordertype) {
        Pageable show_data = simpleStringUtils.getShort(orderby, ordertype, page, size);
        Page<Films> list = null;
        if(name != null && !name.isEmpty()){
            list = filmRepository.findByNameLike("%"+name+"%",show_data);
        }else {
            // nampilkan semuanya
            list = filmRepository.getListData(show_data);
        }
        return new ResponseEntity<Map>(response.templateSukses(list), new HttpHeaders(), HttpStatus.OK);
    }
}
