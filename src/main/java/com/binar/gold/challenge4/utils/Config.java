package com.binar.gold.challenge4.utils;


public class Config {
    public static String SUCCESS_200 = "200";
    public static String ERROR_400 = "400";
    public static String ERROR_401 = "401";
    public static String ERROR_404 = "404";
    public static String ERROR_500 = "500";
}
